<?php

namespace app\modules\api\controllers;

use Yii;
use yii\rest\Controller;
use yii\web\Response;
use yii\web\Request;
use yii\web\HttpException;
use app\models\PgGraphs;
use app\models\PgVertices;
use app\models\PgEdges;

/**
 * Default controller for the `graph` module
 */
class RestController extends Controller
{
    public function beforeAction($action)
    {
        //Устанавливаем формат ответа экшенов контроллера в JSON
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }
    /** 
     * Добавление графа
     * @return JSON
    */
    public function actionAddGraph()
    {
        $response = [];
    	$graph = new PgGraphs();
        $graph->name = Yii::$app->request->getBodyParam('name', 'Unnamed');
    	$graph->save();
    	if(!empty($graph->getErrors()))
            throw new HttpException(500, Yii::t('app', 'Не удалось сохранить'));

        $response['id'] = $graph->id;
        return $response;
    }

    /** 
     * Удаление графа
     * @param $id - идентификатор графа
     * @return JSON
    */
    public function actionDeleteGraph($id)
    {
        $response = [];
        if(!empty($id) && is_numeric($id))
        {
            $idArray = [];
            
            # Удаляем все вершины графа
            $vertices = PgVertices::find()
            ->where([
                'graph_id' => $id
            ])
            ->all();

            foreach ($vertices as $vertex)
            {
                if(!empty($vertex))
                {
                    $idArray[] = $vertex->id;
                    $vertex->delete();
                }
            }

            # Удаляем все рёбра графа
            $edges = PgEdges::find()
            ->where([
                'vertex_from' => $idArray
            ])->orWhere([
                'vertex_to' => $idArray
            ])->all();
            foreach ($edges as $edge)
            {
                if(!empty($edge))
                    $edge->delete();
            }

            # Удаляем граф
            $graph = PgGraphs::findOne($id);
        	if(!empty($graph))
                $graph->delete();
        }
        return $response;
    }

    /** 
     * Добавление вершины
     * @return JSON
    */
    public function actionAddVertex()
    {
        $response = [];
    	$vertex = new PgVertices();
        $params = Yii::$app->request->getBodyParams();
        $vertex->graph_id = $params['graph_id'];
        $vertex->point_x = $params['point_x'];
        $vertex->point_y = $params['point_y'];
        $vertex->save();

        if(!empty($vertex->getErrors()))
            throw new HttpException(500, Yii::t('app', 'Не удалось сохранить'));

        $response['id'] = $vertex->id;
        return $response;
	}

    /** 
     * Удаление вершины
     * @param $id - идентификатор вершины
     * @return JSON
    */
    public function actionDeleteVertex($id)
    {
        $response = [
            'edges' => []
        ];
        if(!empty($id) && is_numeric($id))
        {
	        $vertex = PgVertices::findOne($id);
            if(empty($vertex))
                throw new NotFoundHttpException('Вершина не найдена');
            else
                $vertex->delete();

            $edges = PgEdges::find()
                ->where([
                    "vertex_from" => $id
                ])->orWhere([
                    "vertex_to" => $id
                ])->all();            
            foreach ($edges as $edge)
            {
                $response['edges'][] = $edge->id;
                $edge->delete();
            }
    	}
        return $response;
	}

    /** 
     * Добавление ребра
     * @return JSON
    */
    public function actionAddEdge()
    {
    	$response = [];
        $edge = new PgEdges();
        $params = Yii::$app->request->getBodyParams();
		
        $edge->vertex_from = $params['vertex_from'];
        $edge->vertex_to = $params['vertex_to'];
        $edge->cost = $params['cost'];
        $edge->cost_reverse = $params['cost'];
        $edge->save();

        if(!empty($edge->getErrors()))
            throw new HttpException(500, Yii::t('app', 'Не удалось сохранить'));

        $response['id'] = $edge->id;
        return $response;
    }

    /** 
     * Удаление ребра
     * @param $id - идентификатор ребра
     * @return JSON
    */
    public function actionDeleteEdge($id)
    {
        $response = [];
        $edge = PgEdges::findOne($id);
        if (!empty($edge))
        {
            $edge->delete();
        }
        return $response;
    }

    /** 
     * Получение информации о весе ребра
     * @param $id - идентификатор ребра
     * @return JSON
    */
    public function actionGetCostEdge($id)
    {
        $response = [];
        if(!empty($id) && is_numeric($id))
        {
	        $edge = PgEdges::findOne($id);
	        if (!empty($edge))
	        {	
	        	$response['cost'] = $edge->cost;
	            $response['cost_reverse'] = $edge->cost_reverse;
	        }
            else
            {
                $response['errors'][] = 'Ребро не найдено';
            }
        }
        return $response;
    }

    /** 
     * Изменение информации о весе ребра
     * @param $id - идентификатор ребра
     * @return JSON
    */
    public function actionSetCostEdge($id)
    {
        $response = [];
        $params = Yii::$app->request->getBodyParams();
        $edge = PgEdges::findOne($id);
        if (!empty($edge))
        {
        	$edge->cost = $params['cost'];
            $edge->cost_reverse = $params['cost_reverse'];
            $edge->save();
            $response['errors'] = $edge->getErrors();
        }
        else
            throw new NotFoundHttpException('Ребро не найдено');
        return $response;
    }

    /** 
     * Поиск оптимального маршрута по алгоритму Дейкстры
     * @return JSON
    */
    public function actionFindRouteByDeikstra($id)
    {
        $params = Yii::$app->request->get();
        $fromPoint = $params['from_point'];
        $toPoint = $params['to_point'];
        $response = [];

        if(!empty($id) && is_numeric($id))
        {
            // Выбираем все вершины текущего графа
            $DBVertices = PgVertices::find()
                ->where([
                    "graph_id" => $id,
                ])->all();

            $vertices = [];
            foreach ($DBVertices as $vertex)
            {
                $vertices[] = $vertex->id;
            }

            // Выбираем все ребра полученных вершин
            $DBEdges = PgEdges::find()
                ->where([
                    "vertex_from" => $vertices
                ])
                ->orWhere([
                    "vertex_to" => $vertices
                ])->all();

            // Собираем зависимости и вес
            $neighbours = [];
            foreach ($DBEdges as $edge)
            {
                $neighbours[$edge->vertex_from][] = [
                    "end" => $edge->vertex_to,
                    "cost" => $edge->cost
                ];
                $neighbours[$edge->vertex_to][] = [
                    "end" => $edge->vertex_from,
                    "cost" => $edge->cost_reverse
                ];
            }

            //Устанавливаем начальные значения
            $dist = [];
            foreach ($vertices as $vertex)
            {
                $dist[$vertex] = INF;
                $previous[$vertex] = NULL;
            }
            $dist[$fromPoint] = 0;
            while (count($vertices) > 0)
            {
                $min = INF;
                //Перебираем вершины и находим с минимальной дистанцией
                foreach ($vertices as $vertex)
                {
                    if ($dist[$vertex] < $min)
                    {
                        $min = $dist[$vertex];
                        $u = $vertex;
                    }
                }
                
                // Исключаем её из списка
                $vertices = array_diff($vertices, [$u]);
                if ($dist[$u] == INF or $u == $toPoint)
                    break;
          
                if (isset($neighbours[$u]))
                {
                    foreach ($neighbours[$u] as $arr)
                    {
                        $alt = $dist[$u] + $arr["cost"];
                        if ($alt < $dist[$arr["end"]])
                        {
                            $dist[$arr["end"]] = $alt;
                            $previous[$arr["end"]] = $u;
                        }
                    }
                }
            }
            $path = [];
            $u = $toPoint;
            while (isset($previous[$u]))
            {
                array_unshift($path, $u);
                $u = $previous[$u];
            }
            array_unshift($path, $u);

            $response['items'] = $path;
        }
        return $response;
    }
}
