<?php
$this->title = 'Graph route';
?>
<div class="graph-route-container">
    <div class="row">
        <div class="col-lg-6">

        </div>
        <div class="col-lg-6">
            <div class="graph-control-pane control-pane">
                <fieldset>
                    <legend>Графы</legend>
                    <div class="row">
                        <div class="form-element col-lg-4">
                            <label class="control-label">Название:</label>
                            <input class="form-control" name="name" placeholder="Unnamed">
                        </div>
                        <div class="form-element col-lg-4 non-label">
                            <button id="add-graph" class="btn btn-success">Добавить граф</button>
                        </div>
                        <div class="form-element col-lg-4 non-label">
                            <button id="del-graph" class="btn btn-danger">Удалить граф</button>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="vertices-control-pane control-pane">
                <fieldset>
                    <legend>Вершины</legend>
                    <div class="row">
                        <div class="form-element col-lg-4">
                            <button id="add-vertex" class="btn btn-success">Добавить вершины</button>
                        </div>
                        <div class="form-element col-lg-4">
                            <button id="del-vertex" class="btn btn-danger">Удалить вершины</button>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="edges-control-pane control-pane">
                <fieldset>
                    <legend>Ребра</legend>
                    <div class="row">
                        <div class="form-element col-lg-4">
                            <button id="edge-relation" class="btn btn-success">Связать вершины</button>
                        </div>
                        <div class="form-element col-lg-4">
                            <button id="edge-unrelation" class="btn btn-danger">Отвязать вершины</button>
                        </div>
                    </div>
                </fieldset>
            </div>

            <div class="edges-weight-control-pane control-pane">
                <fieldset>
                    <legend>Вес ребра</legend>
                    <div class="row">
                        <div class="form-element col-lg-4">
                            <label class="control-label">A->B</label>
                            <input class="form-control" name="weight" type="text" value="0">
                        </div>
                        <div class="form-element col-lg-4">
                            <label class="control-label">B->A</label>
                            <input class="form-control" name="weight_reverse" type="text" value="0">
                        </div>
                        <div class="form-element col-lg-4 non-label">
                            <button id="set-weight" class="btn btn-warning">Задать вес вершин</button>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="route-control-pane control-pane">
                <fieldset>
                    <legend>Поиск кратчайшего пути:</legend>
                    <div class="row">
                        <div class="col-lg-4">
                            <label class="control-label">Точка A:</label>
                            <input class="form-control" name="from_point" type="text">
                        </div>
                        <div class="col-lg-4">
                            <label class="control-label">Точка B:</label>
                            <input class="form-control" name="to_point" type="text">
                        </div>
                        <div class="col-lg-4">
                            <div class="btn-top-sp">&nbsp;</div>
                            <button id="find-route" class="btn btn-success">Найти</button>
                        </div>
                    </div>
                    <div class="route-info row">
                        <div class="col-lg-12">
                            <h2>Оптимальный путь:</h2>
                            <div><b class="path"></b></div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</div>