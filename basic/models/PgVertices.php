<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pg_vertexes".
 *
 * @property string $id
 * @property string $graph_id
 * @property double $point_x
 * @property double $point_y
 */
class PgVertices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pg_vertices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['graph_id', 'point_x', 'point_y'], 'safe'],
            [['graph_id', 'point_x', 'point_y'], 'required'],
            [['graph_id'], 'default', 'value' => null],
            [['graph_id'], 'integer'],
            [['point_x', 'point_y'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'graph_id' => 'Graph ID',
            'point_x' => 'Point X',
            'point_y' => 'Point Y',
        ];
    }
}
