<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pg_edges".
 *
 * @property string $id
 * @property string $vertex_from
 * @property string $vertex_to
 * @property double $cost
 */
class PgEdges extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pg_edges';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vertex_from', 'vertex_to'], 'required'],
            [['vertex_from', 'vertex_to'], 'default', 'value' => null],
            [['vertex_from', 'vertex_to'], 'integer'],
            [['cost'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vertex_from' => 'Vertex From',
            'vertex_to' => 'Vertex To',
            'cost' => 'Cost',
        ];
    }
}
