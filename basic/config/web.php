<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'yiigraph',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        "api" => [
            'class' => 'app\modules\api\Module',
        ]
    ],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'YQHNeB9C1mymUVG3qwvvYfcZmMtWj340',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                // Работа с графом
                "POST <controller>/graphs" => "api/<controller>/add-graph",
                "DELETE <controller>/graphs/<id:\d+>" => "api/<controller>/delete-graph",

                // Работа с вершинами
                "POST <controller>/vertices" => "api/<controller>/add-vertex",
                "DELETE <controller>/vertices/<id:\d+>" => "api/<controller>/delete-vertex",

                // Работа с рёбрами
                "POST <controller>/edges" => "api/<controller>/add-edge",
                "DELETE <controller>/edges/<id:\d+>" => "api/<controller>/delete-edge",
                "GET <controller>/edges/<id:\d+>" => "api/<controller>/get-cost-edge",
                "PUT <controller>/edges/<id:\d+>" => "api/<controller>/set-cost-edge",

                // Поиск оптимального пути
                "<controller>/find-route/<id:\d+>" => "api/<controller>/find-route-by-deikstra"
            ],
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
