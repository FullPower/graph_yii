'use strict';

window.graphActionState = 'none';
window.graphID = false;

$(function(){
	// Событие нажатия клавиши добавления графа
	$('#add-graph').on("click", function(){
		__graph_add();
	});

	// Событие нажатия клавиши удаления графа
	$('#del-graph').on("click", function(){
		__graph_delete();
	});

	// Событие нажатия клавиши добавления вершины
	$('#add-vertex').on("click", function (){
		$(this).toggleClass('active');
		window.graphActionState = $(this).hasClass('active') ?'add_vertex' : 'none';
	});

	// Событие нажатия клавиши удаления вершины
	$('#del-vertex').on("click", function (){
		__graph_deleteVertex();
	});

	// Событие нажатия клавиши привязки вершин (добавление ребра)
	$('#edge-relation').on("click", function (){
		__graph_addEdge();
	});

	// Событие нажатия клавиши отвязки вершин (удаление ребра)
	$('#edge-unrelation').on("click", function (){
		__graph_deleteEdge();
	});

	// Событие нажатия клавиши задания веса рёбер
	$('#set-weight').on("click", function (){
		if( $('.vertex-object.active').length == 2 ) {
			var
				vertexFromId = $('.vertex-object.active').eq(0).data('id'),
				vertexToId = $('.vertex-object.active').eq(1).data('id'),
				dataId = $('[data-rel="' + vertexFromId + '' + vertexToId + '"]').data('id');
			sendRequest('rest/edges/' + dataId, 'PUT', {
				vertex_from: vertexFromId,
				vertex_to: vertexToId,
				cost: $('[name="weight"]').val(),
				cost_reverse: $('[name="weight_reverse"]').val(),
			}, function () {

			});
		}
	});

	// Событие нажатия клавиши поиска оптимального пути
	$('#find-route').on("click", function (){
		__graph_findRoute();
	});

	// Событие нажатия мышкой по рабочей области графа
	$('.graph-route-container').on("click", ".graph-object", function (e) {
		switch(window.graphActionState) {
			case 'add_vertex' :
				var
					parentOffset = $(this).parent().offset(),
					pointX = e.pageX - parentOffset.left - 15,
					pointY = e.pageY - parentOffset.top - 15;
					__graph_addVertex(pointX, pointY);
				break;
			default: break;
		}
	});

	// Событие нажатия мышкой по вершине
	$('.graph-route-container').on("click", ".vertex-object", function () {
		$(this).toggleClass('active');

		if( $('.vertex-object.active').length == 2 ) {
			var
				vertexFromId = $('.vertex-object.active').eq(0).data('id'),
				vertexToId = $('.vertex-object.active').eq(1).data('id'),
				dataId = $('[data-rel="' + vertexFromId + '' + vertexToId + '"]').data('id');
			if(dataId)
				sendRequest('rest/edges/' + dataId, 'GET', {}, function(data) {
					if(data.cost && data.cost_reverse) {
						$('[name="weight"]').val(data.cost);
						$('[name="weight_reverse"]').val(data.cost_reverse);
						$('.edges-weight-control-pane .control-label').eq(0).html(vertexFromId +'->'+ vertexToId);
						$('.edges-weight-control-pane .control-label').eq(1).html(vertexToId +'->'+ vertexFromId);
					}
				});
		} else {
			$('[name="weight"]').val(0);
			$('[name="weight_reverse"]').val(0);
			$('.edges-weight-control-pane .control-label').eq(0).html('A->B');
			$('.edges-weight-control-pane .control-label').eq(1).html('B->A');
		}
	});
});

/*
	Rest API send request
*/
function sendRequest(url, method, params, callback_success)
{
	return $.ajax({
		url: "http://" + location.host + "/"+url,
		type: method,
		data: params,
		dataType: "JSON",
		success: callback_success
	});
}

/*
	Добавление графа
*/
function __graph_add()
{
	if($('.graph-route-container .graph-object').length)
		alert('Можно добавить максимум один граф');
	else {
		$('.graph-route-container .row > div').eq(0).append('<div class="graph-object"><svg id="edge-lines"></svg></div>');
		sendRequest('rest/graphs', 'POST', {
			name: $('graph-control-pane [name="name"]').val()
		}, function(params) {
			if(parseInt(params.id))
				window.graphID = params.id;
		});
			
	}
}

/*
	Удаление графа
*/
function __graph_delete()
{
	if($('.graph-route-container .graph-object').length) {
		if(window.graphID) {
			sendRequest('rest/graphs/'+window.graphID, 'DELETE', {}, function(data) {
				$('.graph-route-container .graph-object').remove();
			});
		}
	}
}

/*
	Добавление вершины
*/
function __graph_addVertex(x, y)
{
	sendRequest('rest/vertices', 'POST', {
		graph_id: window.graphID,
		point_x: x,
		point_y: y
	}, function(data) {
		if(parseInt(data.id)) {
			var
				vertexID = data.id;
			$('.graph-route-container .graph-object').append('<div data-id="'+vertexID+'" class="vertex-object">'+vertexID+'</div>');
			$('[data-id="' + vertexID + '"]').css({"left": x, "top": y});
		}
	});
}

/*
	Удаление вершины
*/
function __graph_deleteVertex()
{
	window.graphActionState = 'none';
	//window.graphActionState = 'del_vertex';
	var
		items = $('.graph-route-container .graph-object .vertex-object.active');
	if(items.length) {
		items.each(function(){
			var
				itemId = $(this).data('id');
			if(itemId)
				sendRequest('rest/vertices/'+itemId, 'DELETE', {}, function (data) {
					if(!data.error) {
						items.remove();
					}
					if(data.edges)
						for(var i=0; i<data.edges.length; i++) {
							$('line[data-id="' + data.edges[i] + '"]').remove();
						}
				});
		});
	}
}

/*
	Привязка вершин (добавление ребра)
*/
function __graph_addEdge()
{
	var items = $('.graph-route-container .graph-object .vertex-object.active');
	if(items.length == 2) {
		var
			vertexFromObject = items.get(0),
			vertexToObject = items.get(1),
			vertexFromId = vertexFromObject.getAttribute('data-id'),
			vertexToId = vertexToObject.getAttribute('data-id'),
			x1 = vertexFromObject.offsetLeft + 15,
			y1 = vertexFromObject.offsetTop + 15,
			x2 = vertexToObject.offsetLeft + 15,
			y2 = vertexToObject.offsetTop + 15,
			edgeCost = Math.sqrt( Math.pow(x2-x1, 2) + Math.pow(y2-y1, 2) );

		sendRequest('rest/edges', 'POST', {
			vertex_from: vertexFromId,
			vertex_to: vertexToId,
			cost: edgeCost.toFixed(2)
		}, function (data) {
			if(data.id) {
				var
					line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
				line.setAttribute("data-id", data.id);
				line.setAttribute("data-rel", vertexFromId+''+vertexToId);
				line.setAttribute("x1", x1);
				line.setAttribute("y1", y1);
				line.setAttribute("x2", x2);
				line.setAttribute("y2", y2);
				$('#edge-lines').append(line);
				items.removeClass('active');
			}
		});
	}
}

/*
	Отвязка вершин (удаление ребра)
*/
function __graph_deleteEdge()
{
	var items = $('.graph-route-container .graph-object .vertex-object.active');
	if(items.length == 2) {
	var
		vertexFromObject = items.eq(0),
		vertexToObject = items.eq(1),
		vertexFromId = vertexFromObject.data('id'),
		vertexToId = vertexToObject.data('id'),
		dataId = $('[data-rel="' + vertexFromId + '' + vertexToId + '"]').data('id');
		var
			response = sendRequest('rest/edges/' + dataId, 'DELETE');
		if(response.status == 200) {
			$('[data-id="'+dataId+'"]').remove();
			items.removeClass('active');
		}
	}
}

// Поиск оптимального маршрута
function __graph_findRoute()
{
	$('.route-info').hide();
	$('.vertex-object').removeClass('active');

	sendRequest('rest/find-route/' + window.graphID, 'GET', {
		from_point: $('[name="from_point"]').val(),
		to_point: $('[name="to_point"]').val()
	}, function(data) {
		if(typeof data.items == 'object') {
			var
				path = '';
			for(var item in data.items) {
				var
					itemID = data.items[item];
				path += (!path) ? itemID : ' -> ' + itemID;
				$('[data-id="' + itemID + '"]').addClass('active');
			}
			$('.path').html(path);
			$('.route-info').show();
		}
	});
}